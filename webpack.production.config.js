const webpack = require('webpack');
const fs = require('fs');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const config = require('./webpack.shared.config');
module.exports = config;

// add '.min' before file extension
config.output.filename = config.output.filename.replace(/\.js$/, '.min.js');

config.plugins.push(
	// clean 'dist' folder
	new CleanWebpackPlugin(['dist'], { exclude: 'dist/' }),
	
	// minimize compiled files
	new webpack.optimize.UglifyJsPlugin({
		sourceMap: true
	})
);

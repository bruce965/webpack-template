const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');

process.on('unhandledRejection', (reason, promise) => {
	// faccio in modo che eventuali errori non gestiti facciano chiudere
	// il server piuttosto che lasciarlo in uno stato inconsistente
	throw (reason instanceof Error ? reason : new Error(reason));
});

const app = express();
app.use('/node_modules/', express.static('./node_modules/'));
//app.use('/', express.static('./dist/'));  // commentato perché in sviluppo è più comodo "webpack-dev-middleware"
app.use('/src/', express.static('./src/'));

const webpackConfig = require('./webpack.development.config.js');
app.use(webpackMiddleware(webpack(webpackConfig)));

const server = app.listen(8080, '0.0.0.0', () => console.log(`Server running at "http://${server.address().address}:${server.address().port}/"...`));

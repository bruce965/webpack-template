const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');
const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin');

module.exports = {
	entry: {
		server: './src/_server.ts',
		script: './src/_browser.ts',
		style: './src/_style.less'
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'res/[name].[hash].js',
		libraryTarget: 'umd',
		library: ['fg']
	},
	externals: {
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.less']
	},
	module: {
		rules: [
			{ test: /\.(?:ts|tsx)$/, use: 'ts-loader' },
			{ test: /\.less$/, use: ExtractTextPlugin.extract({ fallback: 'style-loader', use: ['css-loader', 'less-loader'] }) }
		]
	},
	plugins: [
		// extract stylesheets to CSS files
		new ExtractTextPlugin({
			filename: 'res/[name].[contenthash].css',
			allChunks: true
		}),

		// generate pages for static paths
		new StaticSiteGeneratorPlugin({
			entry: 'server'
		}),

		// skip emit some files
		new IgnoreEmitPlugin([
			/(^|\/)server\..*$/,  // do not emit 'server.*'
			/(^|\/)style\..*\.js(\.map)?$/  // do not emit 'style.*.js' and 'style.*.js.map'
		])
	]
};

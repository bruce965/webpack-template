const config = require('./webpack.shared.config');
module.exports = config;

config.devtool = 'source-map';
config.output.devtoolModuleFilenameTemplate = "[resource-path]";
config.output.devtoolFallbackModuleFilenameTemplate = "[resource-path]?[hash]";

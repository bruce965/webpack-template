import * as path from 'path';

const style = require('./_style');

function getAssets(webpackStats: any) {
	const assets: { [extension: string]: { [name: string]: string } } = {};
	for (const k in webpackStats.compilation.assets) {
		const extension = path.extname(k).substr(1);
		const name = path.basename(k).substr(0, path.basename(k).indexOf('.'));
		if (!assets[extension])
			assets[extension] = {};
		
		assets[extension][name] = k;
	}

	return assets;
}

export = async function render(data: { path: string, assets: { [name: string]: string }, webpackStats: any }): Promise<{ [path: string]: string }> {
	const assets = getAssets(data.webpackStats);

	return {
		'/': `<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Test</title>

		<link href="${assets.css.style}" rel="stylesheet">
	</head>
	<body>
		<div class="${style['test-green']}">Path: <span>${data.path}</span></div>

		<script src="${assets.js.script}"></script>
	</body>
</html>`
	};
}
